package ${package};

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** Configuration properties for the application. */
@Configuration
@ConfigurationProperties("byuhbll.${artifactId}")
@Data
public class ${name}Properties {

  // TODO: Add configuration properties as needed.

}
