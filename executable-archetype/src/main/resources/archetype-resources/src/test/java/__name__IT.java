#set($symbol_pound='#')
#set($symbol_dollar='$')
#set($symbol_escape='\')
package ${package};

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

/** Integration tests for the application. */
@SpringBootTest
@ActiveProfiles("test")
public class ${name}IT {

  /** Runs the application. */
  @Test
  public void test() {}
}
