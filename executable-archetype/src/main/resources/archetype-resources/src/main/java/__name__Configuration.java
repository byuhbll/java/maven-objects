package ${package};

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/** Spring bean configuration for the application. */
@Configuration
@AllArgsConstructor
public class ${name}Configuration {

  private ${name}Properties properties;

  // TODO: Add configuration beans as needed.
}